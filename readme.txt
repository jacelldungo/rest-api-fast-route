#Setup

Create a database on Mysql Server named 'routes'

execute the following queries to add tables

CREATE TABLE `_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `_from` varchar(50) NOT NULL,
  `_to` varchar(50) NOT NULL,
  `_time` int(11) NOT NULL,
  `_cost` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

CREATE TABLE `_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;


after adding tables execute the following query to add routes


INSERT INTO `_routes` (id,`_from`,`_to`,`_time`,`_cost`,enabled) VALUES 
(1,'A','C',1,20,1)
,(2,'A','E',30,5,1)
,(3,'A','H',10,1,1)
,(4,'C','B',1,12,1)
,(5,'H','E',30,1,1)
,(6,'E','D',3,5,1)
,(7,'D','F',4,50,1)
,(8,'F','I',45,50,1)
,(17,'F','G',40,50,1)
,(18,'G','B',64,73,1)
(19,'I','B',65,5,1)
;


Execute this query to insert _users

INSERT INTO `_users` (id,username,password,firstname,lastname,`type`,enabled) VALUES 
(1,'jacell','3f05be562f8f93b6f74230906986ee5c','jacell','dungo','admin',1)
,(2,'user1','24c9e15e52afc47c225b757e7bee1f9d','None','None','user',1)
,(3,'admin1','e00cf25ad42683b3df678c61f42c6bda','adminname','adminln','admin',1)
;


How to use?

php -S 127.0.0.1:8080

Open postman and import the file REST API.postman_collection.json located on the root folder

All three request file contains description for their usage