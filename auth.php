<?php 
    require "vendor/autoload.php";
    use \Firebase\JWT\JWT;

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    $email = '';
    $password = '';  

    $servername = "localhost";
    $username = "root";
    $password = "root";
    $dbname = "routes";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $users = array();  

    class Auth
    {
        private function getUsers($username, $password){
            global $conn;
            $sql = "SELECT * from _users u WHERE u.`username` = '".$username."' and password = '".md5($password)."'";
            $result = $conn->query($sql);
            if ($result->num_rows == 1) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    return $row;
                }
            } 

            return FALSE;
        }

        public function validateUser($data){
            $user = $this->getUsers($data->username, $data->password);
            $username = $data->username;
            $password = $data->password;


            if($user !== FALSE){
                echo $this->getToken($user);
            } else {
                http_response_code(401);
                echo json_encode(array("message" => "Login failed.", "username" => $username, "password" => $password));
            }

        }

        private function getToken($data){
            $secret_key = "b55320ae1b6f628506c84dfcfb3ab648";
            $issuer_claim = "api"; // this can be the servername
            $audience_claim = "THE_AUDIENCE";
            $issuedat_claim = time(); // issued at
            $notbefore_claim = $issuedat_claim + 10; //not before in seconds
            $expire_claim = $issuedat_claim + 10000; // expire time in seconds

            $token = array(
                "iss" => $issuer_claim,
                "aud" => $audience_claim,
                "iat" => $issuedat_claim,
                "nbf" => $notbefore_claim,
                "exp" => $expire_claim,
                "data" => array(
                    "firstname" => $data['firstname'],
                    "lastname" => $data['lastname'],
                    "type" => $data['type']
            ));

            http_response_code(200);
            $jwt = JWT::encode($token, $secret_key);
            return json_encode(
                array(
                    "message" => "Successful login.",
                    "jwt" => $jwt,
                    "email" => $data['email'],
                    "expireAt" => $expire_claim
                ));
        }
    }

    $data = json_decode(file_get_contents("php://input"));
    $d = new Auth();
    $d->validateUser($data);
    
?>