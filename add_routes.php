<?php
require "vendor/autoload.php";
use \Firebase\JWT\JWT;

include "make_request.php";

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$request = $_SERVER['REQUEST_URI'];

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "routes";
    // Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}



class Add_routes extends Make_request
{
 

    public function validateAccess($authHeader, $type){
        if($this->validateForm($authHeader, $_POST, $type) == TRUE){
            $this->addRoutes();
        } else{
            http_response_code(401);
            echo "\nSomething went wrong.";
        }
    }

    protected function addRoutes(){
        global $conn;
        if($this->isExisting('add', $_POST) == false){
            $sql = "INSERT INTO _routes (id, _from, _to, _time, _cost, enabled)
            VALUES (DEFAULT, '".$_POST['from']."', '".$_POST['to']."', '".$_POST['time']."', '".$_POST['cost']."', 1)";

            if ($conn->query($sql) === TRUE) {
                echo "Routes Successfully Added!";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

            $conn->close();
        } else {
            echo "Route already existing";
        }
    }


}


$authHeader = $_SERVER['HTTP_AUTHORIZATION'];
$mRequest = new Add_routes();
$mRequest->validateAccess($authHeader, 'add');

?>