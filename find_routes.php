<?php
require "vendor/autoload.php";
use \Firebase\JWT\JWT;

include "make_request.php";

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$request = $_SERVER['REQUEST_URI'];

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "routes";

    // Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

class Find_routes extends Make_request
{
    var $timecost = array();
    var $visited = array();
    var $unvisited = array();
    var $nextbatch = array();
    var $route_history = array();
    var $routes = array();

    public function getRoutes($authHeader, $type){
        if($this->validateForm($authHeader, $_POST, $type) == TRUE){
            $routes = $this->isExisting('get', $_POST);
            if($routes){
                $this->routes = $this->removeDirect($routes, $_POST['from'], $_POST['to']);
                $fastRoute = $this->processDataset('time');
                $lowCost = $this->processDataset('cost');
                $time = $this->runLogic($fastRoute, $_POST['from'], $_POST['to']);
                $cost = $this->runLogic($lowCost, $_POST['from'], $_POST['to']);
                var_dump('fastest route is : ' . implode(' - ', $time));
                var_dump('lowest cost is : ' . implode(' - ', $cost));
            }
        } else{
            http_response_code(401);
            echo "\nSomething went wrong.";
        }
    }

    private function removeDirect($routes, $from, $to) {
        foreach ($routes as $key => $value) {
            if($value['_from'] == $from and $value['_to'] == $to){
                unset($routes[$key]);
            }
        }
        
        return $routes;
    }


    private function processDataset($type){
        $dataset = array();
        foreach ($this->routes as $key => $value) {
            $samp = array();
            if ($type == 'time'){
                array_push($samp, $value['_from']);
                array_push($samp, $value['_to']);
                array_push($samp, $value['_time']);
                array_push($dataset, $samp);
            } else if ($type == 'cost'){
                array_push($samp, $value['_from']);
                array_push($samp, $value['_to']);
                array_push($samp, $value['_cost']);
                array_push($dataset, $samp);
            } 
        }

        return $dataset;

    }


    public function runLogic($graph_array, $source, $target) {
        $vertices = array();
        $neighbours = array();
        foreach ($graph_array as $edge) {
            array_push($vertices, $edge[0], $edge[1]);
            $neighbours[$edge[0]][] = array("end" => $edge[1], "cost" => $edge[2]);
            $neighbours[$edge[1]][] = array("end" => $edge[0], "cost" => $edge[2]);
        }
        $vertices = array_unique($vertices);
     
        foreach ($vertices as $vertex) {
            $dist[$vertex] = INF;
            $previous[$vertex] = NULL;
        }
     
        $dist[$source] = 0;
        $Q = $vertices;
        while (count($Q) > 0) {
     
            // TODO - Find faster way to get minimum
            $min = INF;
            foreach ($Q as $vertex){
                if ($dist[$vertex] < $min) {
                    $min = $dist[$vertex];
                    $u = $vertex;
                }
            }
     
            $Q = array_diff($Q, array($u));
            if ($dist[$u] == INF or $u == $target) {
                break;
            }
     
            if (isset($neighbours[$u])) {
                foreach ($neighbours[$u] as $arr) {
                    $alt = $dist[$u] + $arr["cost"];
                    if ($alt < $dist[$arr["end"]]) {
                        $dist[$arr["end"]] = $alt;
                        $previous[$arr["end"]] = $u;
                    }
                }
            }
        }
        $path = array();
        $u = $target;
        while (isset($previous[$u])) {
            array_unshift($path, $u);
            $u = $previous[$u];
        }
        array_unshift($path, $u);
        return $path;
    }
     


}


$authHeader = $_SERVER['HTTP_AUTHORIZATION'];
$mRequest = new Find_routes();
$mRequest->getRoutes($authHeader, 'get');

?>