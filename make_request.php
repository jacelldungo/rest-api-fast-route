<?php
require "vendor/autoload.php";
use \Firebase\JWT\JWT;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$jwt = null;
$data = json_decode(file_get_contents("php://input"));


class Make_request
{

    var $addRoutesVal = array(
        'from', 'to', 'cost', 'time'
    );
    
    var $findRoutesVal = array(
        'from', 'to'
    );



    private function validateToken($jwt){
        if($jwt){
            $secret_key = "b55320ae1b6f628506c84dfcfb3ab648";
            try {   
                $decoded = JWT::decode($jwt, $secret_key, array('HS256'));
                return $decoded;
            }catch (Exception $e){
                http_response_code(401);
                echo json_encode(array(
                "message" => "Access denied.",
                "error" => $e->getMessage()
            ));
            }
        }
    }

    public function confirmAccess($authHeader){
        $arr = explode(" ", $authHeader);
        $userinfo = $this->validateToken($arr[1]);
        if($userinfo !== FALSE){
            return $userinfo;
        }

        return FALSE;
    }

    public function validateForm($authHeader, $form, $type){
        if ($type == 'add'){
            $dataRequired = $this->addRoutesVal;
        } else {
            $dataRequired = $this->findRoutesVal;
        }


        $userdata = $this->confirmAccess($authHeader);
        if($userdata !== FALSE and $userdata->data->type == 'admin'){

            foreach ($dataRequired as $key => $value) {
                if(!array_key_exists($value, $form)){
                    http_response_code(417);
                    echo json_encode(array("Error" => "Invalid Values", "Required" => json_encode($dataRequired, 1)));
                    return FALSE;
                } else {
                    if(($value == 'from' or $value == 'to') and (!is_string($form[$value]) or strlen($form[$value]) > 1)){
                        http_response_code(417);
                        echo "Invalid Values";
                        return FALSE;
                    } else if(($value == 'cost' or $value == 'time' ) and !is_numeric($form[$value])){
                        http_response_code(417);
                        echo "Integer Required for Time and Cost";
                        return FALSE;
                    }
                }
            }

            return TRUE;
        } else {
            echo "Invalid Access!";
            return FALSE;
        }
    }


    protected function isExisting($type, $form){
        global $conn;

        if ($type == 'add'){
            $sql = "SELECT * from _routes r WHERE r.`_from` = '".$form['from']."' and r.`_to` = '".$form['to']."'";
        } else {
            $sql = "SELECT * from _routes";
        }
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            if ($type == 'add'){
                return TRUE;
            } else{
                return $result->fetch_all(MYSQLI_ASSOC);
            }
        } 

        return FALSE;
    }

}

?>